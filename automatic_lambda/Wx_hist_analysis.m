function [LowFdata, HighFdata_all, HighFdata_independent]  = Wx_hist_analysis(Wavelet_coefficients, Wavelet_book, Wavelet_name)

%%% This function is to have organized the data from wavelet coefficients
%%% for statistical analyses. 

%%% by gab, 2020

    coeffs = Wavelet_coefficients;
    book = Wavelet_book;
    wname = Wavelet_name;
    Nlevel = (size(book,1)-2);
    dtype = {'h','v','d'};
    
    LowFdata = appcoef2(coeffs, book, wname, Nlevel);
    HighFdata_independent = cell(3*Nlevel,1);
    
    HighFdata_all = [];
    cc = 1;
    for nLi = Nlevel:-1:1
        for dci = 1 : 3 % h - v - d
            temp_wx = vec(detcoef2(dtype{dci}, coeffs, book, nLi));
            HighFdata_independent{cc} = temp_wx;
        
            HighFdata_all = [HighFdata_all ; temp_wx];
            cc = cc + 1;
        end
        
    end

end