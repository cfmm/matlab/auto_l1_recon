function [resx,resy,resz] = size(a,n)

if a.adjoint
    res = a.imSize;
else
    res = a.dataSize;
end

if nargout<=1
        if exist('n')
                res = res(n);
        end
    resx = res;
    resy = [];
    resz = [];
elseif nargout==3
    resx = res(1);
    resy = res(2);
    resz = res(3);
else
   error('size return max of 3 values');
end

