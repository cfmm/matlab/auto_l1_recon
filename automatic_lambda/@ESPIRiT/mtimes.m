function res = mtimes(a,x)
% This method applies the ESPIRiT Eigen-Vecs operator

    maps = a.eigenVecs;
    %%% gvm
    if isempty(a.flag_operation)
        [sx,sy,nc,nv,ne] = size(maps);

        if a.adjoint    
           %res = sum(conj(maps).*repmat(x,[1,1,1,nv]),3);    
            res = zeros(sx,sy,1,nv,ne);
            for n=1:nv
                res(:,:,n,:) = sum(conj(maps(:,:,:,n,:)).*x,3);
            end

        else
           res = zeros(sx,sy,nc,1,ne);
           for n=1:nc
               res(:,:,n,:) =  sum(maps(:,:,n,:,:).*x,4);
           end
        end
    else
        %%% its not empty which means that we are working with 3D data...
        %%% gvm
        [sx,sy,sz,nc,nv,ne] = size(maps);

        if a.adjoint    
           %res = sum(conj(maps).*repmat(x,[1,1,1,nv]),3);    
            res = zeros(sx,sy,sz,1,nv,ne);
            for n=1:nv
                res(:,:,:,n,:) = sum(conj(maps(:,:,:,:,n,:)).*x,4);
            end

        else
           res = zeros(sx,sy,sz,nc,1,ne);
           for n=1:nc
               res(:,:,:,n,:) =  sum(maps(:,:,:,n,:,:).*x,5);
           end
        end
    end
       
end

