function  res = ESPIRiT(eigenVecs,  eigenVals,varargin)
%res = ESPIRiT(EigenVecs, [EigenVals,  imSize)
%
%	Implementation of the ESPIRiT maps projection
%

%
% (c) Michael Lustig 2013

%%% modified by gvm
% additional flag indicates my modifications
flag_operation = [];
if ~isempty(varargin)
    flag_operation = 1;
    [sx,sy,sz,nc,nm,ne] = size(eigenVecs);
else
    [sx,sy,nc,nm,ne] = size(eigenVecs);
end

if nargin < 2
    eigenVals = 1;
else
    if isempty(flag_operation)
        eigenVals = repmat(permute(eigenVals,[1,2,4,3,5]),[1,1,nc,1,1]);
    else
        %%% gvm
        eigenVals = repmat(permute(eigenVals,[1,2,3,5,4,6]),[1,1,1,nc,1]);
    end
end


res.eigenVecs = eigenVecs.*sqrt(eigenVals);
res.eigenVals = sqrt(eigenVals);
res.adjoint = 0;
res.flag_operation = flag_operation;
res = class(res,'ESPIRiT');

