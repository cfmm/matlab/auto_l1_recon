function Cropped_Data = nDcrop(Data,Auto_Calibrating_Size, varargin)
%%% This function crops a N-dimensional dataset around its center for the
%%% autocalibrating center. This functions needs that the user specifies
%%% the meaning of each dimension. 

%%% Auto_Calibrating_Size includes k-space + number of coils!!
%%% example: cropND(kspace_data,[20,20,20],'kx','ky','kz','coils','te');
%%% default: assumes te = 1; coils = size(ksp,end); k_{x,y,z} = size(ksp,1:(end-1))

%%% by gvm.

data_size = size(Data);
if numel(data_size) < 1
    disp('No Data size CropND function');
    Cropped_Data = [];
    return;
end
meaning_Dims = [];
valid_argnames = {'kx','ky','kz','coils','te'};
if ~isempty(varargin)
    n = length(varargin);
    meaning_Dims = cell(n,1);
    valid_argnames = {'kx','ky','kz','coils','te'};
    [args_specified,args_location] = ismember(valid_argnames,varargin(1:end));
    
    cc = 1;
    if args_specified(1) == true
        meaning_Dims{cc} = valid_argnames{ args_location(1) };
        cc = cc + 1;
    end
   
    if args_specified(2) == true
        meaning_Dims{cc} = varargin{ args_location(2) };
        cc = cc + 1;
    end

    if args_specified(3) == true
        meaning_Dims{cc} = varargin{ args_location(3) };
        cc = cc + 1;
    end

    if args_specified(4) == true
        meaning_Dims{cc} = varargin{ args_location(4) };
        cc = cc + 1;
    end
    if args_specified(5) == true
        meaning_Dims{cc} = varargin{ args_location(5) };
        cc = cc + 1;
    end
    
    %%% Now, lets check that the dimensions correspond to the dataset
    if numel(data_size) ~= n
        disp('Error: mismatch between Data with supposed dimensions');
        Cropped_Data = [];
        return;
    end
    
else
    meaning_Dims = cell(numel(data_size),1);
    meaning_Dims{end} = 'coils';
    for i = 1:numel(data_size)-1
        meaning_Dims{i} = valid_argnames{i};
    end
end
%disp(meaning_Dims);

%%% Now, lets check that the calibration area corresponds to the k-space
%%% data from all coils.
[~,args_location_coils] = ismember('coils',meaning_Dims);
if numel(Auto_Calibrating_Size) ~= (args_location_coils)
    disp('Error ... mismatch between Autocalibrating Matrix and Data');
    Cropped_Data = [];
    return;
end

%%% Now we can start the cropping procedure :)
k_space_size = data_size(1:(args_location_coils-1));
range = zeros(length(k_space_size),2);
%range_len = zeros(size(k_space_size));
for i = 1:numel(k_space_size)
    % odd or even
    mid = ceil((k_space_size(i)+1)/2);
    ext = Auto_Calibrating_Size(i);
    if mod(k_space_size(i)/2,2) == 0 
        range(i,:) = [(mid -  ceil((ext-1)/2)) , ( mid + ceil((ext-1)/2) -1) ] ;
    else
        range(i,:) = [(mid -  ceil((ext-1)/2)) , ( mid + ceil((ext-1)/2) - 1) ] ;
        %       range(i,:) = [(mid -  ceil((ext-1)/2)) , ( mid + ceil((ext-1)/2) ) ] ;
    end
%    range_len(i) = length(range(i,1):range(i,2));
end
    
    string_2_eval = 'Cropped_Data = Data(';
    for i = 1 : (args_location_coils-1)
        string_2_eval = strcat(string_2_eval,num2str(range(i,1)),':',num2str(range(i,2)),',');
    end
    for i = args_location_coils : numel(data_size)
        string_2_eval = strcat(string_2_eval,':');
        if i ~= numel(data_size)
            string_2_eval = strcat(string_2_eval,',');
        else
            string_2_eval = strcat(string_2_eval,');');
        end
    end
    eval(string_2_eval);
%%%
end
