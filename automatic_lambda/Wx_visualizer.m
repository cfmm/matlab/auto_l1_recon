function [Wx_wd, Wx_img] = Wx_visualizer(Wavelet_coeffcients, Wavelet_book, Wavelet_name)

%%% This function allows to visualize the multi-scale wavelet coefficients
%%% in a simple matrix, Wx_Wd. Wx_img are the set of images for each scale.

%%% INPUT:  1.Wavelet_coefficients
%%%         2.Wavelet_book
%%%         3.Wavelet_name
            %%% NOTE: each input argument are self-explanatory
%%% OUTPUT:
%%%         1. Wx_wd: image containing all wavelet coefficients
%%%         2. Wx_img: resulting image from the wavelet inverse transform
%%%                    using only high-frequency components of a certain level. 
%%%                    (MN x wavelet_levels+1)

%%% log: 2020-03-13: Function working for isotropic and anisotropic iamge sizes.
%%%                  Need to implement Wx_img 
%%%      2020-03-19: Wx_img added! Wx_img in (MN x wavelet_levels+1)


%%% by: gab, 2020

     
    wname = Wavelet_name;
    
    coeffs = Wavelet_coeffcients;
    book = Wavelet_book;
    
    Nlevel = (size(book,1)-2);
    Wx_img = zeros(prod(book(end,:)),Nlevel+1);

    max_bet_levels_x = zeros(Nlevel-1,1);
    max_bet_levels_y = zeros(Nlevel-1,1);
    diff_x = zeros(Nlevel-1,1);
    diff_y = zeros(Nlevel-1,1);

    for i = 3 : (size(book,1)-1)

    if i == 3
        max_bet_levels_x(i-2) = max( [ 2*book(i-1,1)+2 , book(i,1) ] );
        max_bet_levels_y(i-2) = max( [ 2*book(i-1,2)+2 , book(i,2) ] );
    else
        max_bet_levels_x(i-2) = max( [ 2*max_bet_levels_x(i-3)+1 , book(i,1) ] );
        max_bet_levels_y(i-2) = max( [ 2*max_bet_levels_y(i-3)+1 , book(i,2) ] );
    end

    diff_x(i-1) = abs( max_bet_levels_x(i-2) - book(i,1) );
    diff_y(i-1) = abs( max_bet_levels_y(i-2) - book(i,2) );

    end
    %

    total_x = sum(book(1:end-1,1)) + 1;% + sum(max_bet_levels_x)
    total_y = sum(book(1:end-1,2)) + 1;% + sum(max_bet_levels_y)

    %

    draw_lines = 1;
    wavelet_image = zeros( total_x , total_y );
    %

    dtype = {'h','v','d'};
    NLi = Nlevel;
    NLi_book = book( end - NLi , : );
    % low-frequency scale

    wavelet_image( 1:NLi_book(1) , 1:NLi_book(2) ) = reshape(coeffs(1:prod(NLi_book)),NLi_book);
    
    % image from each set of coefficients
    temp_coeffs = zeros(length(coeffs),1);
    temp_coeffs(1:prod(NLi_book)) = coeffs(1:prod(NLi_book));
    multi_scale_counter = 1;
    Wx_img(:,multi_scale_counter) = vec(waverec2(temp_coeffs,book,wname));
    multi_scale_counter = multi_scale_counter + 1;
    
    count_x = NLi_book(1) + 2;
    count_y = NLi_book(2) + 2;

    draw_line_x = fliplr([NLi_book(1) + 1 , max_bet_levels_x' ]);
    draw_line_y = fliplr([NLi_book(2) + 1 , max_bet_levels_y' ]);
    %
    cc = 1;
    %figure,
    %imshow(wavelet_image,[-.5,.5]); colorbar;
    for NLi = Nlevel:-1:1

    NLi_book = book(end-NLi,:);
    %NLi_book
    for dci = 1:3 %dtypes
        switch dci
            %count_x = NLi_book(1) + 1;
            %count_y = NLi_book(2) + 1;
            case 1
                % h
                % +2 is line + first position for the new block
    %                 wavelet_image( 1 : NLi_book(1) , ...
    %                                (NLi_book(2)+1+line_inc_y(cc)) : ((NLi_book(2)+1+line_inc_y(cc))+NLi_book(2)-1) ) = detcoef2(dtype{dci}, coeffs, book, NLi);
                wavelet_image( 1 : NLi_book(1) , ...
                               (count_y) : (count_y + 1*NLi_book(2)-1) ) = detcoef2(dtype{dci}, coeffs, book, NLi);
                %imshow(wavelet_image,[-.5,.5]); colorbar;
            case 2
                % v
                % +2 is line + first position for the new block
                wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
                                1:NLi_book(2) ) = detcoef2(dtype{dci}, coeffs, book, NLi);
            case 3
                % d
                wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
                               (count_y) : (count_y + 1*NLi_book(2)-1) ) = ...
                               detcoef2(dtype{dci}, coeffs, book, NLi);
        end
    end
    % image from each set of coefficients
    temp_coeffs = zeros(length(coeffs),1);
    vert_count = sum( prod( book(1 : (end-NLi-1) , :) , 2 ) );
    temp_coeffs( (vert_count + 1) : (vert_count + 3*prod(NLi_book)) ) = detcoef2('c', coeffs, book, NLi);
    Wx_img(:,multi_scale_counter) = vec(waverec2(temp_coeffs,book,wname));
    multi_scale_counter = multi_scale_counter + 1;
    
    
    count_x = count_x + NLi_book(1) + 1 + diff_x(cc);
    count_y = count_y + NLi_book(2) + 1 + diff_y(cc);

    %imshow(wavelet_image,[-.5,.5]); colorbar;
    if draw_lines == 1
        for lines = 1:2 % x and y axes
            if lines == 1
                wavelet_image( draw_line_x(NLi) ,...
                               1 : 2*draw_line_y(NLi) ) = 1e8;
            else
                wavelet_image( 1 : 2*draw_line_x(NLi) , ...
                               draw_line_y(NLi) ) = 1e8;
            end
        end
    end
    cc = cc + 1;

    end
    % imshow(wavelet_image,[-.5,.5]); colorbar;
    Wx_wd = wavelet_image;
end

