function [USP] = TVD_undersampling_pattern(Matrix_Size,Undersampling_factor,varargin)
%%% Tensor-based variable density function 


%%% by gvm


%%% UNDER DEVELOPMENT
%%% WHAT I NEED TO ADD:
%   1. 3D or even more
%   2. Optional Lambdas and factors for randomness
%   3. Random angles

% DATE LAST CHANGES: 2019/11/04


%%%%%%%%%%%%%%%%
    %%%%%%%%%%
    % Parameters from varargin{'Partial Fourier', A , 'Calibration Lines'}
    % Default Values are empty ...
    PF = [];
    CL = [];
    COILS = [];
    PIx = [];
    PIy = [];
    dPIxy = [];
    Circ_Sampling = [];
    
    if ~isempty(varargin)
        valid_argnames = {'Partial Fourier','Calibration Lines','Coils',...
                          'Parallel Imaging', 'Dephased PI','Circular Sampling'};
        [args_specified,args_location] = ismember(valid_argnames,varargin(1:2:end));
        
        if args_specified(1) == true
            PF = varargin{(args_location(1)*2-1)+1};
        end
        if args_specified(2) == true
            CL = varargin{(args_location(2)*2-1)+1};
        end
        if args_specified(3) == true
            COILS = varargin{(args_location(3)*2-1)+1};
        end
        if args_specified(4) == true
            PIxy = varargin{(args_location(4)*2-1)+1};
            switch PIxy
                case '2x1'
                    PIx = 2; PIy = 1;
                case '1x2'
                    PIx = 1; PIy = 2;
                case '4x1'
                    PIx = 4; PIy = 1;
                case '1x4'
                    PIx = 1; PIy = 4;
                case '2x2'
                    PIx = 2; PIy = 2;
                otherwise
                    disp('Not Implemented yet');
            end
            if args_specified(5) == true
                % NEED TO READ PAPER ABOUT THIS! 
                % I FORGOT THE NAME OF THE PAPER :P
                dPIxy = prod([PIx,PIy]);
                if PIx == 1
                    PIx = 1;
                    PIy = dPIxy;
                else
                    PIy = 1;
                    PIx = dPIxy;
                end
            end
        end
        
        if args_specified(6) == true
            
            if strcmp(varargin{(args_location(6)*2-1)+1},'yes')
                Circ_Sampling = 'yes';
            elseif strcmp(varargin{(args_location(6)*2-1)+1},'no')
                Circ_Sampling = 'no';
            else
                Circ_Sampling = 'no';
            end
            
            
        end
        
    end

    % NUMBER OF DIMENSIONS TO WORK WITH KSPACE DATA
    if numel(Matrix_Size) == 2
        flag_operation = 1;
        
        RotMat = @(ang_)([cos(ang_),-sin(ang_);sin(ang_),cos(ang_)]);
        LambdaMat = @(LD)([LD(1),0;0,LD(2)]);

        eigenDT = @(LL,AA)(RotMat(AA)*LambdaMat(LL)*RotMat(AA)');
        
    elseif numel(Matrix_Size) == 3
        flag_operation = 0;
        
        Rx = @(angXX)([1,0,0;0,cos(angXX),-sin(angXX);0,sin(angXX),cos(angXX)]);
        Ry = @(angYY)([cos(angYY),0,sin(angYY);0,1,0;-sin(angYY),0,cos(angYY)]);
        Rz = @(angZZ)([cos(angZZ),-sin(angZZ),0;sin(angZZ),cos(angZZ),0;0,0,1]);

        rotMat = @(AR)(Rz(AR(1))*Ry(AR(2))*Rx(AR(3)));
        LambdaMat = @(LD)([LD(1),0,0;0,LD(2),0;0,0,LD(3)]);

        eigenDT = @(LL,AA)(rotMat(AA)*LambdaMat(LL)*rotMat(AA)');

    else
        disp(strcat('number of dimensions from kspace data is',num2str(ndims(Kspace_data)),'and it is not implemented yet...'));
        flag_operation = 0;
    end
    
    if flag_operation == 1
        %%% DO TENSOR-BASED VARIABLE DENSITY FUNCTION
        % built-in functions
        
        VEC = @(a_) a_(:);
        
        kc = genNormCoords(Matrix_Size);
        
        ksp_coords = cat(3,reshape(kc(:,1),Matrix_Size) , reshape(kc(:,2),Matrix_Size));
        %kspace = Kspace_data;
        
        USF = Undersampling_factor;

        USP = zeros((Matrix_Size));

        numAngles = 6; % Maybe an arbitrary number of angles
        i_angles = linspace(0,pi-pi/numAngles,numAngles) + (rand()-0.5)*pi;

        % maybe random lambdas too!
        if ~isempty(Circ_Sampling)
            if ndims(Matrix_Size) == 2
                if strcmp(Circ_Sampling,'yes')

                    X= Matrix_Size(1);
                    Y= Matrix_Size(2);

                    midY = ceil((Y-1)/2);

                    if X <= Y
                        midX = ceil((X-1)/2);
                        extX = X-midX;

                        if mod(X,2) == 0
                            x_range = (midX -  extX/2) : ( midX + extX/2 -1) ;
                        else
                            x_range = (midX -  extX/2) : ( midX + extX/2) ;
                        end
                        lambda_1 = max(VEC(sqrt(ksp_coords(x_range,x_range,1).^2 + ksp_coords(x_range,x_range,2).^2)));
                    else
                        midY = ceil((Y-1)/2);
                        extY = Y-midY;
                        if mod(Y,2) == 0
                            y_range = (midY  -  extY/2) : ( midY + extY/2 -1) ;
                        else
                            y_range = (midY  -  extY/2) : ( midY + extY/2) ; % -ceil((Y-1)/2) Again, from left to right
                        end
                        lambda_1 = max(VEC(sqrt(ksp_coords(:,y_range,1).^2 + ksp_coords(:,y_range,2).^2)));
                    end
                    factor = [3e2,5e2,8e2,1e3];
                    % its ok to leave lambda_2 = lambda_1 * 1e-2;
                    lambda_2 = 1e-1*lambda_1;
                else
                    lambda_1 = max(VEC(sqrt(ksp_coords(:,:,1).^2 + ksp_coords(:,:,2).^2)));
                    % its ok to leave lambda_2 = lambda_1 * 1e-3;
                    factor = [1e1,5e1,1e2,5e2];
                    lambda_2 = 1e-3*lambda_1;
                end
            else
                disp('3D not implemented yet');  
            end
        else
            lambda_1 = max(VEC(sqrt(ksp_coords(:,:,1).^2 + ksp_coords(:,:,2).^2)));
            % its ok to leave lambda_2 = lambda_1 * 1e-3;
            factor = [1e1,5e1,1e2,5e2];
            lambda_2 = 1e-3*lambda_1;
        end
        
        Nf = length(factor);
        % lambda_1 goes ]1e0,1e3]
        
         
        %

        ls = [lambda_1,lambda_2];
        Nkc = length(kc);

        for i = 1:numAngles

            i_angle = i_angles(i);
            i_Tensor = zeros(Nkc,1);
            const_fact_i_tensor = factor(randi(Nf));
            for j = 1: Nkc
                i_Tensor(j) = exp((-1*kc(j,:)*eigenDT(const_fact_i_tensor*ls,i_angle)*kc(j,:)'));
            end
            i_Tensor = reshape(i_Tensor,[Matrix_Size]);
            USP = USP + i_Tensor.*((rand(Matrix_Size)-.5).*i_Tensor>0);

            %figure, imshow([USP,i_Tensor],[]);
            %pause(5)
        end

        vUSP = USP(:);
        samples = ceil(numel(USP)/USF);

        [usf_sorted, positions] = sort(vUSP,'descend');
        us_pattern = zeros(prod(Matrix_Size),1);
        us_pattern(positions(1:samples)) = 1;

        USP = squeeze(reshape(us_pattern,[Matrix_Size]));
        %figure, imshow(USP>0,[])
        
        %%% DO EXTRA PROCEDURES
        % order:
        % parallel imaging
        % calibration lines
        % partial fourier
        % number of coils
        if ~isempty(PIx) % One of the two is enough
            % PARALLEL IMAGING - STANDARD
            % 
             if isempty(dPIxy) %dephased is disabled 
                PIxy_matrix = zeros(size(USP));
                PIxy_matrix(1:PIx:end,1:PIy:end) = 1;
                
             else  %dephased is enabled
                 
                 if PIx == 1
                     %%% do dephasing along y
                     axis_acceleration = 2;
                     PIxy_matrix = dephased_PI_matrix(Matrix_Size, PIy, axis_acceleration);
                 else
                     %%% do dephasing along x
                     axis_acceleration = 1;
                     PIxy_matrix = dephased_PI_matrix(Matrix_Size, PIx, axis_acceleration);
                 end
                
             end
            % end
            
            USP = USP.*PIxy_matrix;
        end
        
        if ~isempty(CL)
            % CALIBRATION LINES
            % MAKE SURE THAT THE CALIBRATION LINES ARE ACQUIRED
            
            [X,Y] = size(USP);
            midX = ceil((X-1)/2);
            midY = ceil((Y-1)/2);
            
            if mod(X,2) == 0
                x_range = (midX -  CL/2) : ( midX + CL/2 -1) ;
            else
                x_range = (midX -  CL/2) : ( midX + CL/2) ;
            end
        
            if mod(Y,2) == 0
                y_range = (midY  -  CL/2) : ( midY + CL/2 -1) ;
            else
                y_range = (midY  -  CL/2) : ( midY + CL/2) ; % -ceil((Y-1)/2) Again, from left to right
            end
            
            USP(x_range, y_range) = 1;
           
            % REAL QUESTION: ONLY LEFT TO RIGHT?
        end
        
        if ~isempty(PF)
            % PARTIAL FOURIER
            % ERASE PHASE ENCODING LINES FROM LEFT TO RIGHT
            
            zz = round(PF*size(USP,2)); 
            USP(:,1:zz) = 0;
            
            % REAL QUESTION: ONLY LEFT TO RIGHT?
            % REAL QUESTION: ROUND OR CEIL???
        end
        
        combined_acceleration_factor = numel(USP)/sum(VEC(USP));
        disp(strcat('combined Acceleration Factor:',num2str(combined_acceleration_factor)));
        
        if ~isempty(COILS)
            dimss = size(USP);
            vec_dimss = zeros(numel(dimss)+1,1);
            vec_dimss(1:numel(dimss)) = 1;
            vec_dimss(end) = COILS;
            USP = repmat(USP , vec_dimss');
        end
        
    else
        %%% SAFE ESCAPE MODE:
        %%% SOMETHING IS WRONG OR NOT IMPLEMENTED YET ... SKIP
        % not implemented yet
        USP = [];
    end

end

function [dephased_PI, dephasing] = dephased_PI_matrix(Matrix_Size, Acceleration_Factor, Axis_Acceleration)
%%% breuer, 2006 CAIPRINHA
    

    R = Acceleration_Factor;
    dephasing = randi([1,R-1]);
    dephasing_axis = Axis_Acceleration;
    
    pattern = zeros(Matrix_Size);
    Nx = Matrix_Size(1);
    Ny = Matrix_Size(2);
    
    switch dephasing_axis
        case 1
            pattern(1:R:Nx,:) = 1;
            %figure, imagesc(pattern);
            for x = 1 : R : Nx
                for y = 1 : R : Ny

                    mini_pattern = pattern( x : x+R-1 , y : y+R-1 );
                    last_position = 0;
                    for i = 2 : R
                        mini_pattern(:,i) = circshift(mini_pattern(:,i),dephasing+last_position,1);
                        last_position = mod(dephasing+last_position,R);
                    end
                    pattern( x : x+R-1 , y : y+R-1 ) = mini_pattern;
                end
            end
            %figure, imagesc(pattern);
        case 2
            pattern(:,1:R:Ny) = 1;
            %%%
            %figure, imagesc(pattern);
            for x = 1 : R : Nx
                for y = 1 : R : Ny

                    mini_pattern = pattern( x : x+R-1 , y : y+R-1 );
                    last_position = 0;
                    for i = 2 : R
                        mini_pattern(i,:) = circshift(mini_pattern(i,:),dephasing+last_position,2);
                        last_position = mod(dephasing+last_position,R);
                    end
                    pattern( x : x+R-1 , y : y+R-1 ) = mini_pattern;
                end
            end
            %figure, imagesc(pattern);
          %%%

        otherwise
    end

    dephased_PI = pattern;
end