function [sized_EigenVecs, sized_EigenVals] = nDkernelEig(kernel, imSize)
% [eigenVecs, eigenVals] = kernelEig(kernel, imSize)
%
% Function computes the ESPIRiT step II -- eigen-value decomposition of a 
% k-space kernel in image space. Kernels should be computed with dat2Kernel
% and then cropped to keep those corresponding to the data space. 
%
% INPUTS:
%           kernel - k-space kernels computed with dat2Kernel (5D)
%                   [size_window_x , size_window_y , size_window_z , number of coils , number_of_relevant_eigenvectors]
%           imSize - The size of the image to compute maps for [sx,sy,sz]
%
% OUTPUTS:
%           EigenVecs - Images representing the Eigenvectors. (sx,sy,sz,Num coils,Num coils)
%           EigenVals - Images representing the EigenValues. (sx,sy,sz,numcoils )
%                       The last are the largest (close to 1)
%           
% 
% See Also:
%               dat2Kernel
% 
%
% (c) Michael Lustig 2010

% Initial Dimensions of the kernel
% [size_window_x , size_window_y , size_window_z , number of coils , number_of_relevant_eigenvectors]
% imSize [sx,sy,sz]

nc = size(kernel,4);
nv = size(kernel,5);
kSize = [size(kernel,1), size(kernel,2), size(kernel,3)];

% "rotate kernel to order by maximum variance"
k = permute(kernel,[1,2,3,5,4]); k =reshape(k,prod([kSize,nv]),nc);

if size(k,1) < size(k,2)
    [u,s,v] = svd(k);
else 
    [u,s,v] = svd(k,'econ');
end

k = k*v;
kernel = reshape(k,[kSize,nv,nc]); kernel = permute(kernel,[1,2,3,5,4]);

%%% pre-compute image size reduction
%%% allocate memory according to reducted image size
%levels = 2;

%sub_im = datapyd(nDzpad(conj(kernel(end:-1:1,end:-1:1,end:-1:1,1,1)), ...
%        [imSize(1), imSize(2),imSize(3)]),'image','Gaussian_pyramid',numel(imSize),-1*levels,[],[]);
size_sub_im = ceil(imSize/4); %clear sub_im;

%KERNEL = zeros([size_sub_im,nc,nv]);
% step_iv = find(mod(nv,(1:10))==0,1,'last'); %1:18 given my experience
% for iv = 1:step_iv:nv
%     a = datapyd(nDzpad(conj(kernel(end:-1:1,end:-1:1,end:-1:1,:,iv:(step_iv+iv-1)))*sqrt(prod(imSize)), ...
%                  [imSize(1), imSize(2),imSize(3)]),'fourier','Gaussian_pyramid',numel(imSize),-1*levels,[],[]);
%     KERNEL(:,:,:,:,iv:(step_iv+iv-1)) = fft3c(fft3c(a)); 
% end

%%% going to comment the following to check improvements
% a = nDzpad(conj(kernel(end:-1:1,end:-1:1,end:-1:1,:,:)), ...
%             [size_sub_im(1), size_sub_im(2),size_sub_im(3)]);
% S = size(a);
% b = reshape(a,[prod(S(1:(end-1))),S(end)]);
% mu = mean(mean(abs(b),2));
% sigma = std(mean(abs(b),2)); 
% clear b;
% Gauss_kernel = eval_with_complex_gauss( genNormCoords(squeeze(size_sub_im)), 1 , (sigma/mu)^2);
% Gauss_kernel = reshape(Gauss_kernel,size_sub_im);
% s_end = prod(S((numel(size_sub_im)+1):end));
% a = reshape(a,[size_sub_im,s_end]);
% for i = 1:s_end
%     a(:,:,:,i) = a(:,:,:,i).*Gauss_kernel*sqrt(prod(size_sub_im));
% end
% a = reshape(a,S);
% KERNEL = fft3c(a); 
% clear a; clear Gauss_kernel;
% %%% going to comment the following to check improvements
% 
% %for n=1:size(kernel,5)
%     %%% reduction
% %    a = nDzpad(conj(kernel(end:-1:1,end:-1:1,end:-1:1,:,n))*sqrt(imSize(1)*imSize(2)*imSize(3)), ...
% %        [imSize(1), imSize(2),imSize(3)]);
% %    sub_im = datapyd(fftnc(a),'fourier','Gaussian_pyramid',numel(imSize),-1*levels,[],[]);
%     %%% store
% %    KERNEL(:,:,:,:,n) = (fft3c(sub_im));
% %end

KERNEL = zeros(size_sub_im(1), size_sub_im(2), size_sub_im(3), size(kernel,4), size(kernel,5));
for n=1:size(kernel,5)
    KERNEL(:,:,:,:,n) = (fft3c(nDzpad(conj(kernel(end:-1:1,end:-1:1,end:-1:1,:,n))*sqrt(size_sub_im(1)* size_sub_im(2)* size_sub_im(3)), ...
        [size_sub_im(1), size_sub_im(2), size_sub_im(3)])));
end

KERNEL = KERNEL/sqrt(prod(kSize));

EigenVecs = zeros(size_sub_im(1), size_sub_im(2), size_sub_im(3), nc, min(nc,nv));
EigenVals = zeros(size_sub_im(1), size_sub_im(2), size_sub_im(3), min(nc,nv));

for n=1:prod(size_sub_im)
    [x,y,z] = ind2sub([size_sub_im(1),size_sub_im(2),size_sub_im(3)],n);
    mtx = squeeze(KERNEL(x,y,z,:,:));

    %[C,D] = eig(mtx*mtx');
    [C,D,V] = svd(mtx,'econ');
    
    ph = repmat(exp(-i*angle(C(1,:))),[size(C,1),1]);
    C = v*(C.*ph);
    D = real(diag(D));
    EigenVals(x,y,z,:) = D(end:-1:1);
    EigenVecs(x,y,z,:,:) = C(:,end:-1:1);
end

%%% expand EigenVectors and EigenVals
% just use the last one anyway because the rest was going to be discarded
% after the function.
EigenVals = EigenVals(:,:,:,end);
EigenVecs = EigenVecs(:,:,:,:,end);
% need to analize here what is going on! 
[X,Y,Z] = meshgrid(linspace(1,size_sub_im(2),imSize(2)),linspace(1,size_sub_im(1),imSize(1)),linspace(1,size_sub_im(3),imSize(3)));
%
%sized_EigenVals =   interp3(EigenVals, levels, 'cubic');   
sized_EigenVals =   interp3(EigenVals, X,Y,Z, 'cubic');   

sized_EigenVecs = zeros([imSize,size(EigenVecs,4)]);

for ni = 1:size(EigenVecs,4)
    sized_EigenVecs(:,:,:,ni) = interp3(EigenVecs(:,:,:,ni), X,Y,Z, 'cubic');
end

end