function [Handle] = av_histogram(Data, varargin)
%%% this function generates a multicolor histogram.
%%% an optional value can be used to split the histogram. Otherwise, we
%%% split 1-4 and 2-3 Quartiles. 

%%% ArtVis toolbox (c) gab,2020

%%% log: 2020-03-19: 


    N = 256;
    data = Data(:);
    mu_ = mean(data);
    std_ = std(data);
    
    [count, bins] = genHistogram(data,N);
    count = count/max(count);
    max_delta = 1.05;
    
    % split colours
    cc = 1;
    qn = 2;
    current_ax = [];
    quartiles = [];
    flag_lambda = false;
    flag_axes = false;
    
    if ~isempty(varargin)
        for i = 1: 2 :length(varargin)
            if strcmp(varargin{i},'lambda')
                lambda = varargin{i+1};
                qn = 2;
                quartiles = [mu_ - lambda, mu_ + lambda];
                flag_lambda = true;
                pos_quartiles = zeros(length(quartiles), 1);
                iter = 2;
                while cc < (qn+1)
                    if quartiles(cc) >= bins(iter-1) && quartiles(cc) < bins(iter)
                        pos_quartiles(cc) = iter-1;
                        cc = cc + 1;
                    end
                    iter = iter + 1;
                end
            end
            if strcmp(varargin{i},'axes')
                current_ax = varargin{i+1};
                flag_axes = true;
            end
        end
    end
    if flag_lambda == false
        quartiles = [ mu_ - std_ , mu_ + std_ ];
        pos_quartiles = zeros(qn, 1);
        
        for iter = 1 : qn
            [pos] = find( quartiles(iter) >= bins , 1 ,'last');
            pos_quartiles(iter) = pos;
        end
    end
    intervals = [1; pos_quartiles ;N];
    orange_ = [0.85,0.325,0.098];
    %yellow_ = [0.9290, 0.6940, .1250];
    
    yellow_ = [1,1,0];
    green_ = [0.4660,0.6740,0.1880];
    purple_ = [0.4940, 0.1840, 0.5560];
    blue_ = [0, 0.4470, 0.7410];
    colours = {yellow_,blue_,yellow_};
    %colours = {purple, orange, purple}; % purple / orange
    if flag_axes == false

        current_ax = axes;
    end
    axes(current_ax);
    hold on,
    cc = 1;
    for iter = 2 : length(intervals) 
        range_plot = intervals(iter-1):intervals(iter);
        
        % PLOT BAR
        bar(bins(range_plot),count(range_plot),'FaceColor',colours{cc},'EdgeColor',colours{cc},'LineWidth',1.5);
        
        % DRAW AREA
        bina = bins(intervals(iter-1));
        binb = bins(intervals(iter));
        x_patch = [bina,binb,binb,bina];
        y_patch = [0, 0, max_delta,max_delta];
        patch(x_patch,y_patch,...
                colours{cc},'FaceAlpha',.15,...
                'EdgeColor',colours{cc},'EdgeAlpha',0.15);
        cc = cc + 1;  
    end
    if flag_axes == false
        for iter = [2,3]
            %%% DRAW VERTICAL LINE
            binb = bins(intervals(iter));
            plot(current_ax,[binb, binb],...
                 [0, max_delta],'LineStyle','--','LineWidth',2,'Marker','none','Color','k');
        end
    end
    Handle = [bins(intervals(2)), bins(intervals(3))];
        
    hold off;
    if Gini_Index(data) > 0.65
        axis(current_ax,[bins(1),bins(end),0, max_delta]); % uniform distributed data
        %axis([bins(1),bins(end),0, mean(count)+0.25*mean(count)]); % impulse distributed data
    else
        axis(current_ax,[bins(1),bins(end),0, max_delta]); % uniform distributed data
    end

    if flag_axes == true
        set(gca, 'LineWidth', 1, 'YTick',[], 'Xtick',[]);
    else
        set(gca, 'LineWidth', 1, 'FontSize', 20);
        ylabel(strcat('$','p(x)','$'),'Interpreter','Latex');
        xlabel(strcat('$','x','$'),'Interpreter','Latex');
    end
end

function [Count, Bins] = genHistogram(Data, N)

    Maxd = max(Data);
    Mind = min(Data);
    Bins = linspace(Mind-.1*Mind, Maxd+.1*Maxd, N);
    
    Count = zeros(N,1);
    for iter = 2 : N
       Count(iter-1) = sum((Data >= Bins(iter-1)) .* (Data < Bins(iter)) ); 
    end
end

function GI = Gini_Index(SetCoefficients)

    s = sort(abs(SetCoefficients(:)),'ascend');
    N = length(s);
    s = reshape(s,[1,N]);
    v = 1:N;
    l1norm = norm(s,1);
   
    
    GI = 1 - 2*sum( (s / l1norm ) .* ( (N-v+0.5)/N )  );

end