function [Sum_Complex_Gauss] = eval_with_complex_gauss(Kspace_Coordinates,Centre_Gaussians, Vars_Gaussians)
%%% This functions evaluates kspace coordinates for complex Gaussian data as:
%%% 
%%%     Sum_Complex_Gauss = Re{gauss} + Im{ sum_{ i = dims-1} gauss_i}
%%% 
%%% where 1 gaussian is for the real data (normally, readout direction) and 1 or 2
%%% gaussians as imaginary data (normally, phase and partition directions).
%%% In this way, it is possible to simulate any kind of complex data and
%%% knowing somewhat the ground truth.
%%%

%%% by gvm, 2019

%%% INPUT:
%%%
%%% Kspace_Coordinates: identifies kspace coordinates for evaluation
%%%                     Vector of Matrix containing MxN coordinates. M is
%%%                     the number of coordinates to evaluate and N corresponds to 
%%%                     the number of dimensions and 1 < N < 3; 
%%%           
%%% Centre_Gaussians: identifies the centre of each Gaussian for the
%%%                   evaluation at a corresponding position. Nx1 Matrix
%%%
%%% Vars_Gaussians: identigies the variance for each Gaussian. Nx1 Matrix
%%%

%%% log:

%%% 2019-11-27:  Changed a little bit the function for real Gaussian only.
%%%              Perhaps is necessary to do the same for imaginary Gaussians.
n_dim_coords = size(Kspace_Coordinates,2);
n_dimensions = length(Centre_Gaussians);
operation_flag = 1;
switch n_dimensions
    case 1
        %%% real Gaussian only
        gauss = @(coords_ ,mid_ , var_) exp( - ( diag(coords_ * diag(repmat(mid_,[1,n_dim_coords])).^2 * coords_' ) ./ (2*var_)));
    case 2
        %%% real Gaussian + imag Gaussian
        gauss = @(coords_ ,mid_ , var_) exp( - ( (coords_(:,1) - mid_(1)).^2 / (2*var_(1)) + (coords_(:,2) - mid_(2)).^2 / (2*var_(2)) ) ) + ...
                                     1i*exp( - ( (coords_(:,1) - mid_(1)).^2 / (2*var_(1)) + (coords_(:,2) - mid_(2)).^2 / (2*var_(2)) ) );
    case 3
        %%% real Gaussian + imag Gaussian 1 + imag Gaussian 2 
        gauss = @(coords_ ,mid_ , var_) ... 
            exp( - ( ((coords_(:,1) - mid_(1)).^2 / (2*var_(1))) + ((coords_(:,2) - mid_(2)).^2 / (2*var_(1))) + ((coords_(:,3) - mid_(3)).^2 / (2*var_(1))) )) + ...
         1i*exp( - ( ((coords_(:,1) - mid_(1)).^2 / (2*var_(2))) + ((coords_(:,2) - mid_(2)).^2 / (2*var_(2))) + ((coords_(:,3) - mid_(3)).^2 / (2*var_(2))) )) ;

        %gauss = @(coords_ ,mid_ , var_) exp( - ( diag(coords_ * diag(repmat(mid_,[1,n_dim_coords])).^2 * coords_' ) ./ (2*var_)));
        %gauss = @(coords_ ,mid_ , var_) exp( - ( diag(coords_ * .^2 * coords_' ) ./ (2*var_)));
    
    otherwise
        %%% Not implemented
        operation_flag = 0;    
end
    %%% Do Evaluation
    if operation_flag == 1
        NN = size(Kspace_Coordinates,1);
        step_max = find(mod(NN,(1:140))==0,1,'last');
        Sum_Complex_Gauss = zeros(NN,1);
        for posi = 1:step_max:NN
            Sum_Complex_Gauss(posi:(posi+step_max-1)) = gauss(Kspace_Coordinates(posi:(posi+step_max-1),:), Centre_Gaussians, Vars_Gaussians);
        end
    else
        Sum_Complex_Gauss = [];
    end
    
end