function [Wx_wd, Wx_imgs] = av_Wx_visualizer(Wavelet_coeffcients, Wavelet_book, Wavelet_name)

%%% colours
orange_ = [0.85,0.325,0.098];
%yellow_ = [0.9290, 0.6940, .1250];
yellow_ = [1,1,0];
green_ = [0.4660,0.6740,0.1880];
purple_ = [0.4940, 0.1840, 0.5560];
blue_ = [0, 0.4470, 0.7410];


%%% See original function Wx_visualizer for details
    % W_imgs = {Wx_img_low , Wx_img , Wx_img}
    
    wname = Wavelet_name;
    
    coeffs = Wavelet_coeffcients;
    book = Wavelet_book;
    RGB = 3;
    red = 1;
    green = 2;
    
    Nlevel = (size(book,1)-2);

    max_bet_levels_x = zeros(Nlevel-1,1);
    max_bet_levels_y = zeros(Nlevel-1,1);
    diff_x = zeros(Nlevel-1,1);
    diff_y = zeros(Nlevel-1,1);

    for i = 3 : (size(book,1)-1)

    if i == 3
        max_bet_levels_x(i-2) = max( [ 2*book(i-1,1)+2 , book(i,1) ] );
        max_bet_levels_y(i-2) = max( [ 2*book(i-1,2)+2 , book(i,2) ] );
    else
        max_bet_levels_x(i-2) = max( [ 2*max_bet_levels_x(i-3)+1 , book(i,1) ] );
        max_bet_levels_y(i-2) = max( [ 2*max_bet_levels_y(i-3)+1 , book(i,2) ] );
    end

    diff_x(i-1) = abs( max_bet_levels_x(i-2) - book(i,1) );
    diff_y(i-1) = abs( max_bet_levels_y(i-2) - book(i,2) );

    end
    %

    total_x = sum(book(1:end-1,1)) + 1*numel(diff_x) + sum(diff_x);% + sum(max_bet_levels_x)
    total_y = sum(book(1:end-1,2)) + 1*numel(diff_y) + sum(diff_y);% + sum(max_bet_levels_y)

    %

    draw_lines = 1;
    % wavelet_image = .5*ones( total_x , total_y , RGB); %% this is the lineeee!
    wavelet_image = zeros( total_x , total_y , RGB); %% this is the lineeee!
    Wx_imgs = zeros(prod(book(end,:)), RGB , Nlevel + 1 );
    %

    dtype = {'h','v','d'};
    NLi = Nlevel;
    NLi_book = book( end - NLi , : );
    % low-frequency scale

    wavelet_image( 1:NLi_book(1) , 1:NLi_book(2), : ) = repmat(reshape(coeffs(1:prod(NLi_book)),NLi_book),[1,1,RGB]);
    % rescaling the app wavelet coefficients
    wavelet_image( 1:NLi_book(1) , 1:NLi_book(2), : ) = wavelet_image( 1:NLi_book(1) , 1:NLi_book(2), : ) / max(max(max( wavelet_image( 1:NLi_book(1) , 1:NLi_book(2), : ))));
    
    
    % image from each set of coefficients
    temp_coeffs = zeros(length(coeffs),1);
    temp_coeffs(1:prod(NLi_book)) = coeffs(1:prod(NLi_book));
    multi_scale_counter = 1;
    Wx_imgs( : , : , multi_scale_counter) = repmat(vec(waverec2(temp_coeffs,book,wname)),[1,RGB,1]);
    multi_scale_counter = multi_scale_counter + 1;
    
    count_x = NLi_book(1) + 2;
    count_y = NLi_book(2) + 2;

    draw_line_x = fliplr([NLi_book(1) + 1 , max_bet_levels_x' ]);
    draw_line_y = fliplr([NLi_book(2) + 1 , max_bet_levels_y' ]);
    %
    cc = 1;
    %figure,
    %imshow(wavelet_image,[-.5,.5]); colorbar;
    figure,
    ax1 = axes('Position',[0.4,0.05,0.7,.7]);
    ax2 = axes('Position',[0.25,0.5,.4,.4]);
    ax3 = axes('Position',[0.01,0.7,.3,.3]);
    
    lambda_zeros = [-1e-3, 1e-3];
    axes_container = {ax3,ax2,ax1};
    for NLi = Nlevel:-1:1

    NLi_book = book(end-NLi,:);
    
    % image from each set of coefficients
    temp_coeffs_in = zeros(length(coeffs),1);
    temp_coeffs_out = zeros(length(coeffs),1);
    vert_count = sum( prod( book(1 : (end-NLi-1) , :) , 2 ) );
    
    % lambdas for splitting wavelet coefficients
    [lambdas] = av_histogram( detcoef2('c', coeffs, book, NLi) ,'axes',axes_container{NLi});
    
    temp_level_coeffs = detcoef2('c', coeffs, book, NLi);
    mask_temp = ( ( lambdas(1) <= temp_level_coeffs ) .* ( temp_level_coeffs <= lambdas(2) ) );
    temp_coeffs_in( (vert_count + 1) : (vert_count + 3*prod(NLi_book)) ) =  (mask_temp) .* temp_level_coeffs;
    anato_red = vec(waverec2(temp_coeffs_in,book,wname));
    Wx_imgs(: , red , multi_scale_counter) = anato_red;
    temp_coeffs_out( (vert_count + 1) : (vert_count + 3*prod(NLi_book)) ) = (~mask_temp) .* temp_level_coeffs;
    anato_green = vec(waverec2(temp_coeffs_out,book,wname));
    Wx_imgs(: , green , multi_scale_counter) = anato_green;
    
%     mask_zeros = logical((anato_red==0) .* (anato_green==0));
%     Wx_imgs(mask_zeros,:,multi_scale_counter) = 0.5;
    
    
    for dci = 1:3 %dtypes
        switch dci
            %count_x = NLi_book(1) + 1;
            %count_y = NLi_book(2) + 1;
            case 1
                % h
                
                % split coefficients according level lambdas
                coeffs_dci = detcoef2(dtype{dci}, coeffs, book, NLi);
                mask_temp = ( ( lambdas(1) <= coeffs_dci ) .* ( coeffs_dci <= lambdas(2) ) );
          
                % re-scale coefficients to improve visualization
                coeffs_dci = coeffs_dci / max(coeffs_dci(:));
                
                %mask_zeros = ( ( lambda_zeros(1) <= coeffs_dci ) .* ( coeffs_dci <= lambda_zeros(2) ) );
                % in
                for pn = 1 : RGB
                    wavelet_image( 1 : NLi_book(1) , ...
                                (count_y) : (count_y + 1*NLi_book(2)-1), pn ) = ((mask_temp) .* coeffs_dci) * blue_(pn);
                end
                % out
                for pn = 1 : RGB
                wavelet_image( 1 : NLi_book(1) , ...
                               (count_y) : (count_y + 1*NLi_book(2)-1), pn ) = ((~mask_temp) .* coeffs_dci)*yellow_(pn);
                end
               % zeros to grey
%                wavelet_image( 1 : NLi_book(1) , ...
%                                (count_y) : (count_y + 1*NLi_book(2)-1), : ) = repmat((mask_zeros) .* 0.5,[1,1,RGB]);
%                 
            case 2
                % v
                % split coefficients according level lambdas
                coeffs_dci = detcoef2(dtype{dci}, coeffs, book, NLi);
                mask_temp = ( ( lambdas(1) <= coeffs_dci ) .* ( coeffs_dci <= lambdas(2) ) );
                 
                % re-scale coefficients to improve visualization
                coeffs_dci = coeffs_dci / max(coeffs_dci(:));
                %mask_zeros = ( ( lambda_zeros(1) <= coeffs_dci ) .* ( coeffs_dci <= lambda_zeros(2) ) );
                % in
                for pn = 1 : RGB
                wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
                                1:NLi_book(2) , pn ) = ((mask_temp) .* coeffs_dci)*blue_(pn);
                            
                end
                % out
                for pn = 1 : RGB
                wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
                                1:NLi_book(2) , pn ) = ((~mask_temp) .* coeffs_dci)*yellow_(pn);
                end
                            % zeros to grey
%                wavelet_image(  (count_x) : (count_x + 1*NLi_book(1)-1) , ...
%                                 1:NLi_book(2) , : ) = repmat((mask_zeros) .* 0.5,[1,1,RGB]);
             case 3
                % d
                % split coefficients according level lambdas
                coeffs_dci = detcoef2(dtype{dci}, coeffs, book, NLi);
                mask_temp = ( ( lambdas(1) <= coeffs_dci ) .* ( coeffs_dci <= lambdas(2) ) );
                 
                % re-scale coefficients to improve visualization
                coeffs_dci = coeffs_dci / max(coeffs_dci(:));
                %mask_zeros = ( ( lambda_zeros(1) <= coeffs_dci ) .* ( coeffs_dci <= lambda_zeros(2) ) );
                % in
                for pn = 1 : RGB
                wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
                               (count_y) : (count_y + 1*NLi_book(2)-1) , pn) = ((mask_temp) .* coeffs_dci)*blue_(pn);
               
                end
                %  out
                for pn = 1 : RGB
                wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
                               (count_y) : (count_y + 1*NLi_book(2)-1) , pn) = ((~mask_temp) .* coeffs_dci)*yellow_(pn);
                end          
              % zeros to grey
%                 wavelet_image( (count_x) : (count_x + 1*NLi_book(1)-1) , ...
%                                (count_y) : (count_y + 1*NLi_book(2)-1) , : ) = repmat((mask_zeros) .* 0.5,[1,1,RGB]);
         end
    end
    
    multi_scale_counter = multi_scale_counter + 1;
    
    count_x = count_x + NLi_book(1) + 1 + diff_x(cc);
    count_y = count_y + NLi_book(2) + 1 + diff_y(cc);

    %imshow(wavelet_image,[-.5,.5]); colorbar;
    if draw_lines == 1
        for lines = 1:2 % x and y axes
            if lines == 1
                wavelet_image( draw_line_x(NLi) ,...
                               1 : 2*draw_line_y(NLi) , : ) = 1e8;
            else
                wavelet_image( 1 : 2*draw_line_x(NLi) , ...
                               draw_line_y(NLi) , : ) = 1e8;
            end
        end
    end
    cc = cc + 1;



    end
    % imshow(wavelet_image,[-.5,.5]); colorbar;
    Wx_wd = wavelet_image;


end