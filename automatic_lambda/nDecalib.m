function [Maps, Weights] = nDecalib(K_Space_Data, Number_Calibration_Lines, Kernel_Size)

%%% log: 2019-11-22
% Functions Verified so far
% nDcrop
% nDim2row
% nDdat2Kernel(calib,ksize);

%%% log: 2019-12-01
% Functions Verified so far
% nDkernelEig
% and everything appears to work :)

 eigThresh_k = 0.03; % threshold of eigenvectors in k-space
 eigThresh_im = 0.9; % threshold of eigenvectors in image space

ksp = K_Space_Data;
ncalib = Number_Calibration_Lines;
ksize = Kernel_Size;

% Generate ESPIRiT Maps (Takes 30 secs to 1 minute)
switch numel(Kernel_Size) 
    case 2
        [sx,sy,Nc] = size(ksp);
        calib = crop(ksp,[ncalib,ncalib,Nc]);

        % Get maps with ESPIRiT
        [kernel,S] = dat2Kernel(calib,ksize);
        idx = find(S >= S(1)*eigThresh_k, 1, 'last' );

        [M,W] = kernelEig(kernel(:,:,:,1:idx),[sx,sy]);
        Maps = M(:,:,:,end);

        % Weight the eigenvectors with soft-sense eigen-values
        Weights = W(:,:,end) ;
        Weights = (Weights - eigThresh_im)./(1-eigThresh_im).* (W(:,:,end) > eigThresh_im);
        Weights = -cos(pi*Weights)/2 + 1/2;
        
    case 3
        [sx,sy,sz,Nc] = size(ksp);
        calib = nDcrop(ksp,[ncalib,ncalib,ncalib,Nc],'kx','ky','kz','coils');
        
         % Get maps with ESPIRiT
         switch 0
             case 0
                [kernel,S] = nDdat2Kernel(calib,ksize);
             case 1
                [kernel,S] = nDdat2Kernel(calib,ksize);
                save('kernels_2020_01_24.mat');
             case 2
                 load kernels_2020_01_24.mat;
                 
         end

        idx = find(S >= S(1)*eigThresh_k, 1, 'last' );
        
        [Maps,Weights_i] = nDkernelEig(kernel(:,:,:,:,1:idx),[sx,sy,sz]);

        % Weight the eigenvectors with soft-sense eigen-values  
        Weights = (Weights_i - eigThresh_im)./(1-eigThresh_im).* (Weights_i > eigThresh_im);
        Weights = -cos(pi*Weights)/2 + 1/2;

end