function [Calibration_Matrix]  = nDim2row(K_Space_Data,Kernel_Size)

ker = Kernel_Size;
dat = K_Space_Data;

switch numel(Kernel_Size)
    case 2
        %%%
        [sx,sy,sz] = size(dat);

        res = zeros((sx-ker(1)+1)*(sy-ker(2)+1),prod(ker),sz);
        count=0;
        for y=1:ker(2)
            for x=1:ker(1)
                count = count+1;
                res(:,count,:) = reshape(dat(x:sx-ker(1)+x,y:sy-ker(2)+y,:),...
                    (sx-ker(1)+1)*(sy-ker(2)+1),1,sz);
            end
        end  
        %%%
    case 3
        %%%
                %%%
        [sx,sy,sz,sc] = size(dat);

        res = zeros((sx-ker(1)+1)*(sy-ker(2)+1)*(sy-ker(2)+1),prod(ker),sc);
        count=0;
        for z = 1:ker(3)
            for y=1:ker(2)
                for x=1:ker(1)
                    count = count+1;
                    res(:,count,:) = reshape(dat(x:sx-ker(1)+x,y:sy-ker(2)+y,z:sz-ker(3)+z,:),...
                        (sx-ker(1)+1)*(sy-ker(2)+1)*(sz-ker(3)+1),1,sc);
                end
            end
        end
        %%%
        
    otherwise
        dips('nDim2row(ksp,kernel) is Not Implemented Yet for input arguments! Sorry!');
        Calibration_Matrix = [];
        return;
end
Calibration_Matrix = res;
end