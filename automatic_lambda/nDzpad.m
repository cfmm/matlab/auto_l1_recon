function Zpad_Data = nDzpad(Cropped_Data,Size_Original, varargin)

%%% by gvm.


numel_4_zpad = numel(Size_Original);
diff_additional_dimensions = numel(size(Cropped_Data)) - numel_4_zpad;

mid_ori = ceil((Size_Original+1)/2);
ext_w = zeros(numel_4_zpad,2);
n_ext = length(ext_w);

switch numel(Size_Original)
    case 2
        switch diff_additional_dimensions
            case 0
                Zpad_Data = zeros(Size_Original);

                cpd = [size(Cropped_Data,1),size(Cropped_Data,2),size(Cropped_Data,3)];
                for i = 1:n_ext

                    if mod(size(Cropped_Data,i),2) == 0
                    % even
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)-1];
                    else
                    % odd
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)];
                    end
                end
                Zpad_Data(ext_w(1,1):ext_w(1,2),    ...
                             ext_w(2,1):ext_w(2,2), :) = Cropped_Data;            
            case 1

                Zpad_Data = zeros([Size_Original,...
                    size(Cropped_Data,numel_4_zpad+1)]);

                cpd = [size(Cropped_Data,1),size(Cropped_Data,2),size(Cropped_Data,3)];
                for i = 1:n_ext

                    if mod(size(Cropped_Data,i),2) == 0
                    % even
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)-1];
                    else
                    % odd
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)];
                    end
                end
                Zpad_Data(ext_w(1,1):ext_w(1,2),    ...
                             ext_w(2,1):ext_w(2,2), :) = Cropped_Data;
            case 2

                Zpad_Data = zeros([Size_Original,     ...
                    size(Cropped_Data,numel_4_zpad+1),...
                    size(Cropped_Data,numel_4_zpad+2)]);

                cpd = [size(Cropped_Data,1),size(Cropped_Data,2),size(Cropped_Data,3)];
                for i = 1:n_ext

                    if mod(size(Cropped_Data,i),2) == 0
                    % even
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)-1];
                    else
                    % odd
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)];
                    end
                end
                Zpad_Data(ext_w(1,1):ext_w(1,2),...
                             ext_w(2,1):ext_w(2,2),:,:) = Cropped_Data;
        end
        
    case 3
        
        switch diff_additional_dimensions
            case 0
                Zpad_Data = zeros(Size_Original);

                cpd = [size(Cropped_Data,1),size(Cropped_Data,2),size(Cropped_Data,3)];
                for i = 1:n_ext

                    if mod(size(Cropped_Data,i),2) == 0
                    % even
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)-1];
                    else
                    % odd
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)];
                    end
                end
                Zpad_Data(ext_w(1,1):ext_w(1,2),    ...
                             ext_w(2,1):ext_w(2,2), ...
                             ext_w(3,1):ext_w(3,2), :) = Cropped_Data;            
            case 1

                Zpad_Data = zeros([Size_Original,...
                    size(Cropped_Data,numel_4_zpad+1)]);

                cpd = [size(Cropped_Data,1),size(Cropped_Data,2),size(Cropped_Data,3)];
                for i = 1:n_ext

                    if mod(size(Cropped_Data,i),2) == 0
                    % even
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)-1];
                    else
                    % odd
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)];
                    end
                end
                Zpad_Data(ext_w(1,1):ext_w(1,2),    ...
                             ext_w(2,1):ext_w(2,2), ...
                             ext_w(3,1):ext_w(3,2), :) = Cropped_Data;
            case 2

                Zpad_Data = zeros([Size_Original,     ...
                    size(Cropped_Data,numel_4_zpad+1),...
                    size(Cropped_Data,numel_4_zpad+2)]);

                cpd = [size(Cropped_Data,1),size(Cropped_Data,2),size(Cropped_Data,3)];
                for i = 1:n_ext

                    if mod(size(Cropped_Data,i),2) == 0
                    % even
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)-1];
                    else
                    % odd
                    ext_w(i,:) = [mid_ori(i)-ceil((cpd(i)-1)/2),mid_ori(i)+ceil((cpd(i)-1)/2)];
                    end
                end
                Zpad_Data(ext_w(1,1):ext_w(1,2),...
                             ext_w(2,1):ext_w(2,2),...
                             ext_w(3,1):ext_w(3,2),...
                             :,:) = Cropped_Data;
        end

end