**Package for wavelet-based compressed sensing MRI reconstructions. Supports:**
  - Cartesian Multi-channel data

**Contact Info**
- Gabriel Varela Mattatall, 
  email: gabriel.varela.mattatall@gmail.com; gvarela2@uwo.ca
  twitter: @gabvarelam1

**Log Summary**
- 2021-03-30: Creation of the Repository.

**Acknowledgements**
Please cite the following papers when using this repository:
- G. Varela-Mattatall et al. Automatic Determination of the Regularization weighting for wavelet-based compressed sensing MRI reconstructions. MRM, 2021.
- F. Ong et al. General phase regularized reconstruction using phase cycling. MRM, 2018.

**Requirements**
- Matlab's Wavelet Toolbox

**Before Usage**
  - run demo.m to have a feeling of what is going on.
  - explore the rest of the folders.

**Notes**
  - Trying to debug everything. Suggestions and feedback are welcome!


(c) 2021, Gabriel Varela Mattatall
