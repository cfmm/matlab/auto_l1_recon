function [Handle] = av_spider(Data, Data_Labels, Data_Legends)
%%% this function plots a spider plot:
%%% INPUTS:
%%%         1. Data: MxN: M number of axis    class matrix
%%%                       N number of cases

%%%         2. Data_Labels: names for cases   class cell
%%%                         
%%%                     NOTE: ** length(Data_Labels) == N

%%%         3. Data_Legends: Names for each set

%%% OUTPUT: 
%%%         1. Handle: [FOR NOW] just a 1-0 identifier to know if the
%%%         operation was successful or not. 
%%%
%%%
%%% log: 2020-03-21 working v.1 

%%% todo:   use maximum area when maxeasy4 is <<< 1
%%%         add std as circles? 
%%%         add auto labels and auto legends
%%%         do it for any number of strategies
%%%         sort strategies according to max? 
%%%         make text for latex :D

operation_flag = 1;
%%% Verification steps
if length(Data_Labels) == 0
    operation_flag = 0;
end

if isempty(Data)
    operation_flag = 0;
else
    if size(Data,2) < 3
        disp('number of cases is too low. Please consider another type of plot...');
    end
    if size(Data,1) < 2
        disp('Number of axis is too low. Please consider another type of plot...')
    end
end

if operation_flag == 1
    % proceed to plot the spider plot

    f = figure;
    ca = gca(f);
    cla(ca); 
    
    nLabels = size(Data,1);
    nMeasures = size(Data,2);
    
    %orange_ = [0.85,0.325,0.098];
    yellow_ = [0.9290, 0.6940, .1250];
    green_ = [0.4660,0.6740,0.1880];
    purple_ = [0.4940, 0.1840, 0.5560];
    blue_ = [0, 0.4470, 0.7410];
    colours = {yellow_,green_,blue_,purple_};
    
    
    hold on;
    set(f,'color','w')
    % set the axes to the current text axes
    axes(ca)
    % set to add plot
    set(ca,'nextplot','add');

    % clear figure and set limits
    set(ca,'visible','off'); set(f,'color','w')
    set(ca,'xlim',[-1.25 1.25],'ylim',[-1.25 1.25]); axis(ca,'equal','manual')

        % traditional legend
    for ii = 1 : nMeasures
         text(-2., 1.25 - (ii-1)*0.25, ...
                    char(strcat('${',Data_Legends{ii},'}$')), ...
                    'FontSize',32,'Interpreter','Latex','Color',colours{ii},'FontWeight','bold',...
                    'Units','data');
  
    end



    markers_per_axis = [.25,.5,.75,1];
    angles_per_axis = linspace(0, 2*pi, nLabels + 1);
    delta = 0.0125;
    %
    %%% patches
    start = cos(angles_per_axis');
    stop = sin(angles_per_axis');
    for iter = 2 : 2 : length(markers_per_axis)
        start1 = [(start(1)*markers_per_axis(iter-1));(start*markers_per_axis(iter));...
                  (start*markers_per_axis(iter-1));(start(1)*markers_per_axis(iter))];% , start*markers_per_axis(iter)];
        stop1 = [(stop(1)*markers_per_axis(iter-1));(stop*markers_per_axis(iter));...
                  (stop*markers_per_axis(iter-1));(stop(1)*markers_per_axis(iter))];
        patch(ca,start1,stop1,[.5,.5,.5],'FaceAlpha',.1,'EdgeColor','none'); axis equal

    end
    
    %%% axis
    start = [zeros(nLabels,1)' ; cos(angles_per_axis(1:(end-1)))];
    stop = [zeros(nLabels,1)' ; sin(angles_per_axis(1:(end-1)))];
    plot(ca,start,stop,'color','k','linestyle','-'); axis equal
    maxValues = max(Data');
    
    maxeasy4 = zeros(length(maxValues),1);
    for i = 1 : length(maxValues)
        if max(maxValues(i)) < 1
            maxeasy4(i) = (ceil(100*maxValues(i)/4)*4)/100;
        else
            maxeasy4(i) = ceil(maxValues(i)/4)*4;
        end
    end
    
    for ii = 1 : length(angles_per_axis)-1
        % markers
        plot(ca,[[cos(angles_per_axis(ii)) * markers_per_axis + sin(angles_per_axis(ii)) * delta]; ...
                [cos(angles_per_axis(ii)) * markers_per_axis - sin(angles_per_axis(ii)) * delta]], ...
                [[sin(angles_per_axis(ii)) * markers_per_axis - cos(angles_per_axis(ii)) * delta] ;
                [sin(angles_per_axis(ii)) * markers_per_axis + cos(angles_per_axis(ii)) * delta]],'color','k','LineWidth',.5);

        % max at each axis    

        % flip the text alignment for right side axes
        if angles_per_axis(ii) > pi/2 && angles_per_axis(ii) < pi
            % only 1 quadrant: top left
            temp = text([cos(angles_per_axis(ii)) * 1.2 + sin(angles_per_axis(ii)) * 0], ...
            [sin(angles_per_axis(ii)) * 1.2 - cos(angles_per_axis(ii)) * 0], ...
            num2str(maxeasy4(ii)), ...
            'fontsize',22);
        else
            % rest of quadrants
            temp = text([cos(angles_per_axis(ii)) * 1.1 + sin(angles_per_axis(ii)) * 0], ...
            [sin(angles_per_axis(ii)) * 1.1 - cos(angles_per_axis(ii)) * 0], ...
            num2str(maxeasy4(ii)), ...
            'fontsize',22);
        end
        
        % flip the text alignment for lower axes
        if angles_per_axis(ii) >= pi
            set(temp,'HorizontalAlignment','right')
        end

        % label each axis
        temp = text([cos(angles_per_axis(ii)) * 1.4 + sin(angles_per_axis(ii)) * 0], ...
                [sin(angles_per_axis(ii)) * 1.4 - cos(angles_per_axis(ii)) * 0], ...
                char(strcat('$',Data_Labels{ii},'$')), 'FontSize',28,'Interpreter','Latex');

        % flip the text alignment for right side axes
        if angles_per_axis(ii) > pi/2 && angles_per_axis(ii) < 3*pi/2
            set(temp,'HorizontalAlignment','right')
        end

    end
    %%% areas
%     start = cos(angles_per_axis');
%     stop = sin(angles_per_axis');
%     for iter = 1 : nLabels
%         for inc = markers_per_axis
%             plot(ca,start*inc,stop*inc,'color','k','linestyle','-'); axis equal
%         end
%     end

    %%% measurements and legends
    normData = Data ./ repmat(maxeasy4,[1,size(Data,2)]);
    
    o = polar(ca,angles_per_axis()'*ones(1,nMeasures),[normData;normData(1,:)]);
    % angles shifted to insert legends between axes
    angles_bet_axis = linspace( mean(angles_per_axis(1:2))  , 2*pi + mean(angles_per_axis(1:2)), nMeasures+1 );
   
    for ii = 1 : nMeasures
        %colour
        set(o(ii),'color',colours{ii},'LineWidth',4);
        % legend
        
        % when you want legends to rotate in the plot
        %if angles_bet_axis(ii) > pi/2 && angles_bet_axis(ii) < 3*pi/2
        %    temp = text([cos(angles_bet_axis(ii)) * 1.5 + sin(angles_bet_axis(ii)) * 0], ...
        %        [sin(angles_bet_axis(ii)) * 1.5 - cos(angles_bet_axis(ii)) * 0], ...
        %        char(strcat('${',Data_Legends{ii},'}$')), 'FontSize',32,'Interpreter','Latex','Color',colours{ii},'FontWeight','bold');
        %else
        %    temp = text([cos(angles_bet_axis(ii)) * 1.1 + sin(angles_bet_axis(ii)) * 0], ...
        %            [sin(angles_bet_axis(ii)) * 1.1 - cos(angles_bet_axis(ii)) * 0], ...
        %            char(strcat('${',Data_Legends{ii},'}$')), 'FontSize',32,'Interpreter','Latex','Color',colours{ii},'FontWeight','bold');
        %end

    end

    
else
    % do nothing
end

Handle = 1;

end