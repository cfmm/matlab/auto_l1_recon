function pcc = pcCoeff(recon,gt)
% this function obtains the person correlation coefficient. The idea from
% this funtion is to analyse the structural similarity between the
% reconstruction and the ground truth.

%(C) (Paquette, 2015)
% Implemented by gv

n = length(gt(:));
Zrecon = Zwuju(recon);
Zgt = Zwuju(gt);
pcc = (1/(n-1))*(Zrecon'*Zgt);

end

function Zx = Zwuju(x)
    Zx = (x(:) - mean(x(:)))/std(x(:));
end