function [nD_Kernel,S] = nDdat2Kernel(Data,AC_Kernel)

data = Data;
kSize = AC_Kernel;

[sx,sy,sz,nc] = size(data);

tmp = nDim2row(data,kSize); [tsx,tsy,tsz,tsc] = size(tmp);
A = reshape(tmp,tsx,tsy*tsz*tsc);

[U,S,V] = svd(A,'econ');
    
nD_Kernel = reshape(V,kSize(1),kSize(2),kSize(3),nc,size(V,2));
S = diag(S);S = S(:);

end