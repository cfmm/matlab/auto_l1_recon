function [list_lambdas] = selection_adaptive_lambda(x0 , wname, N, Outlier_Percentage, signal_dims)
%%% this functions determines for the initial zero-filled image the
%%% approximate values for the threshold in each decomposition level
    

    switch signal_dims
        case 1
            list_lambdas =  helper1(x0 , wname, N, Outlier_Percentage);
        case 2
            list_lambdas =  helper2(x0 , wname, N, Outlier_Percentage);
        case 3
            list_lambdas =  helper3(x0 , wname, N, Outlier_Percentage);
        otherwise
            disp("Unknown number of dimensions to define a wavelet proximal operator");
            list_lambdas = [];
    end

end

function List_of_Lambdas = helper1(signal, wname, N, outliers_percentage )
    %%% built-in functions for the job!
    % trimmed mean and std for statistical separation
    sort2trim = @(data_) sort( data_(:) );
    trim = @(data_ , percentage_out_) data_(ceil(length(data_)*percentage_out_ + .001):ceil(length(data_)*(1-percentage_out_)));
    trim_mean = @(data_ , delta_ ) mean( trim( sort2trim(data_) , delta_ ) );
    trim_std = @(data_ , delta_ ) std( trim( sort2trim(data_) , delta_ ) );
    
    
    set_range = @(Book_,Extraction_Number_)(1 + sum(Book_(1:(Extraction_Number_-1))) : sum(Book_(1:Extraction_Number_)));
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % [signal, r] = randshift(signal);
    % dwtmode('ppd', 0);

    s = size(signal);
    
    n_dims = 1;
    
    for i = 1:prod(s(2:end))

        % real part
        List_of_Lambdas = zeros(1+N,prod(s(2:end)));
    
        %rj_data = ...
        %        nD_extract_coefficients_from_wavelets(coeffs_real, book_real, n_dims , 1 , wname, []);
    
        %mu_m = trim_mean( rj_data , outliers_percentage );
        %sigma_m = trim_std( rj_data , outliers_percentage );
        %th_Lambda = (mu_m + 3*sigma_m); % 99.7 percent of trimmed coefficients. 
                                              % low damage to signal coefficients
         
        List_of_Lambdas(1,i) = 0;     % we dont touch low frequencies
        
        [coeffs_real, book_real] = wavedec(real(signal(:,i)),N,wname);

        
        for rj = 2:N+1

            rj_data = ...
                nD_extract_coefficients_from_wavelets(coeffs_real, book_real, n_dims , rj , wname, []);

            mu_m = trim_mean( rj_data , outliers_percentage );
            sigma_m = trim_std( rj_data , outliers_percentage );
            th_Lambda = (mu_m + 3*sigma_m); % 99.7 percent of trimmed coefficients. 
                                                  % low damage to signal coefficients
                                                  % to aggresive for L1
                                                  %
            List_of_Lambdas(rj) = th_Lambda;
            %coeffs_real( set_range( book_real , rj )) = SoftThresh( rj_data, th_Lambda );
        end

        % imaginary part
        %%% I assume that there is no imaginary part for now
    end

    %signal_rec = randunshift( signal_rec, r );

end

function [Approx_factor, Fact_1 , Fact_2, Fact_3, Fact_4, Init_index] = define_factors(Extraction_Number, Detail_Type)

    Init_index = 2; 
    switch Extraction_Number
        case 1
            % just approx coefficients
            Fact_1 = 1;
            Fact_2 = 1;
            Init_index = 1;
            Approx_factor = 0;
        case 2
            % boundary between approx and detail coefficients
            Fact_1 = 0;
            Fact_2 = 3;
            Approx_factor = 1;
        otherwise
            % detail coefficients
            Fact_1 = 3;
            Fact_2 = 3;
            Approx_factor = 1;
    end
    
    if strcmp(Detail_Type,'H')
        Fact_3 = 0;
        Fact_4 = 1;
    elseif strcmp(Detail_Type,'V')
        Fact_3 = 1;
        Fact_4 = 2;
    elseif strcmp(Detail_Type,'D')
        Fact_3 = 2;
        Fact_4 = 3;        
    else
        disp('Unknown behaviour for L1 proximal operator');
        Fact_3 = 0;
        Fact_4 = 0;  
    end

end

function List_of_Lambdas = helper2(signal, wname, N, outliers_percentage)


    %%% built-in functions for the job!
    sort2trim = @(data_) sort( data_(:) );
    trim = @(data_ , percentage_out_) data_(ceil(length(data_)*percentage_out_ + .001):ceil(length(data_)*(1-percentage_out_)));
    trim_mean = @(data_ , delta_ ) mean( trim( sort2trim(data_) , delta_ ) );
    trim_std = @(data_ , delta_ ) std( trim( sort2trim(data_) , delta_ ) );
    %discard_percentage = .15; %.15
    
    s = size(signal);
    List_of_Lambdas = zeros(1+N,3);
    
    List_of_Lambdas(1,:) = 0;
    
    n_dims = 2;
    dtype = {'H','V','D'};
    
     % real part
    List_of_Lambdas = zeros(1+N,prod(s(3:end)));

    for i = 1:prod(s(3:end))
        
        % real part
        [coeffs_real, book_real] = wavedec2(real(signal(:,:,i)),N,wname);
    
        rj_data = ...
                nD_extract_coefficients_from_wavelets(coeffs_real, book_real, n_dims , 1 , 'both', []);
            
        mu_m = trim_mean( rj_data , outliers_percentage );
        sigma_m = trim_std( rj_data , outliers_percentage );
        th_Lambda = (mu_m + 3*sigma_m); % 99.7 percent of trimmed coefficients. 
                                              % low damage to signal coefficients
        List_of_Lambdas(1,i) = th_Lambda; 
        %List_of_Lambdas(1,i) = 0;     % we dont touch low frequencies

        
        for rj = 2:N+1
            rj_per_level = [];
            for rj2 = 1:3
                %[approx_factor , fact_1 , fact_2, fact_3, fact_4, init_index] = define_factors(rj, dtype{rj2});

                %set_ranges_2D = @(Book , Extraction_Number) ...
                %    (1 + approx_factor*sum(prod(Book(1,:),2)) + fact_1*sum(prod(Book(init_index:(Extraction_Number-1),:),2)) + ...
                %                           fact_3*sum(prod(Book(Extraction_Number,:),2))) : ...
                %    ( approx_factor*sum(prod(Book(1,:),2)) + fact_2*sum(prod(Book(init_index:Extraction_Number-1,:),2)) + ...
                %                           fact_4*sum(prod(Book(Extraction_Number,:),2)));
                
                %disp(length(set_ranges_2D(book_real,rj)));
                
                rj_data = nD_extract_coefficients_from_wavelets(coeffs_real, book_real, n_dims , rj, 'both', dtype{rj2});
                
                rj_per_level = [rj_per_level, rj_data];
                 
            
            end
            mu_m = trim_mean( rj_per_level , outliers_percentage );
            sigma_m = trim_std( rj_per_level , outliers_percentage );
            th_Lambda = (mu_m + 3*sigma_m); % 99.7 percent of trimmed coefficients. 
                                                  % low damage to signal coefficients
            
            List_of_Lambdas(rj,i) = th_Lambda;
        end
        
        
    end

    %signal_rec = randunshift( signal_rec, r );


end

function  List_of_Lambdas = helper3(signal, wname, N, outliers_percentage)
%%% built-in functions for the job!
    %%% built-in functions for the job!
    sort2trim = @(data_) sort( data_(:) );
    trim = @(data_ , percentage_out_) data_(ceil(length(data_)*percentage_out_ + .001):ceil(length(data_)*(1-percentage_out_)));
    trim_mean = @(data_ , delta_ ) mean( trim( sort2trim(data_) , delta_ ) );
    trim_std = @(data_ , delta_ ) std( trim( sort2trim(data_) , delta_ ) );
    %discard_percentage = .15; %.15
    
    s = size(signal);

List_of_Lambdas = zeros( N*7 + 1 , 1 );

for i = 1:prod(s(4:end))
    
    % real part
    real_stwave = wavedec3(real(signal(:,:,:,i)),N,wname);
    
    Nreal = length(real_stwave.dec);
    for rj = 2:Nreal
%        size_real = size(real_stwave.dec{rj});
        rj_data = vec(real_stwave.dec{rj});

        mu_m = trim_mean( rj_data , outliers_percentage );
        sigma_m = trim_std( rj_data , outliers_percentage );
        th_Lambda = (mu_m + 3*sigma_m); % 99.7 percent of trimmed coefficients. 
                                              % low damage to signal coefficients
        List_of_Lambdas(rj) = th_Lambda; 
        
        %real_stwave.dec{rj} = reshape( SoftThresh( rj_data, Lambda ), size_real );
    end
          
end

%signal_rec = randunshift( signal_rec, r );

end