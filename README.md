**Package for wavelet-based compressed sensing MRI reconstructions. Supports:**
  - Cartesian Multi-channel data
  - Wave encoding (See CS-Wave project where main functions were slightly changed)

**Contact Info**
- Gabriel Varela Mattatall, 
  email: gabriel.varela.mattatall@gmail.com; gvarela2@uwo.ca
  twitter: @gabvarelam1

**Log Summary**
- 2021-03-30: Creation of the Repository.
- 2022-05-03: Updated functions to improve reconstruction performance.
		- We changed a little bit the way to determine the boundary between wavelet coefficients.
		- After testing it on spirals/corkscrew trajectories + virtual coils, we suggest to use 1 wavelet level.  		

**Acknowledgements**
Please cite the following papers when using this repository:
- G. Varela-Mattatall et al. Automatic Determination of the Regularization weighting for wavelet-based compressed sensing MRI reconstructions. MRM, 2021.
- F. Ong et al. General phase regularized reconstruction using phase cycling. MRM, 2018.

**Requirements**
- Matlab's Wavelet Toolbox

**Notes**
  - Trying to debug everything. Suggestions and feedback are welcome!


(c) 2021, Gabriel Varela Mattatall
